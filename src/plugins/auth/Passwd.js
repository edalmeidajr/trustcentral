import React from 'react';

const Passwd = (
	<div>
		<div className="form-group">
			<input className="form-control" type="text" placeholder="Username or Email" />
		</div>
		<div className="form-group">
			<input className="form-control" type="text" placeholder="Password" />
		</div>
		<div className="form-group">
			<button className="btn btn-primary btn-block">Enter</button>
		</div>
		<div className="form-group">
			<a className="btn btn-default btn-block">Clear</a>
		</div>
		<div className="form-group">
			<a href="/register">Don't have an account? Then register!</a>
		</div>
		<a href="/recover">Forget your password?</a>
	</div>
)

export default Passwd;