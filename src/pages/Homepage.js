import React from 'react';

import PageContainer from '../containers/PageContainer';

import HomepageMenu from '../components/Homepage/HomepageMenu';
import HomepageContents from '../components/Homepage/HomepageContents';

export default class Homepage extends React.Component {
	render() {
		return(
			<PageContainer menu={<HomepageMenu />} contents={<HomepageContents />} />
		)
	}
}
