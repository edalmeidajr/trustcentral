import React from 'react';

export default class PageContainer extends React.Component {
	render() {
		return(
			<div className="App">
			    <section className="App-header">
			        <span className="App-title">
			            <span className="App-title-white">Trust</span>
			            <span className="App-title-yellow">Central</span>
			        </span>
			        <div className="App-top-menu">
			        	{this.props.menu}
			        </div>
			    </section>
			    <section className="App-contents">
			        {this.props.contents}
			    </section>
			</div>
		)
	}
}
