import React from 'react';

import '../../assets/styles/HomepageContents.css';

export default class HomepageContents extends React.Component {
	render() {
		return(
			<div className="HomepageContents">
				<section className="HomepageContents-top">
					<div className="row">
						<div className="col-md-6">
							<div className="well justified">
								<div className="title-destak">A higher security level with a lower effort level</div>
								Data security is an issue
							</div>
						</div>
					</div>
				</section>
				<section className="HomepageContents-explaining">
					<div className="row">
						<div className="col-md-6">
							<div className="standard-frame HomepageContents-explaining-item HomepageContents-explaining-item-01">
								<div className="title-destak">Security</div>
								Achieve a higher security level by controlling all your servers and users in a single tool which allows you to easily implement as many security levels as you want, manage rules and particular permissions and much more..
							</div>
						</div>
						<div className="col-md-6">
							<div className="standard-frame HomepageContents-explaining-item HomepageContents-explaining-item-02">
								<div className="title-destak">Digital Certificates</div>
								Use Digital Certificates to make your communications much more secure. Create your own certificates easily or use those provided by security companies.
							</div>
						</div>
						<div className="col-md-6">
							<div className="standard-frame HomepageContents-explaining-item HomepageContents-explaining-item-03">
								<div className="title-destak">Public/Private Keys</div>
								Improve security allowing access on a per-machine basis with public/private keypairs. Generate, import, export and manage those keypairs as easily as a mouse click.
							</div>
						</div>
						<div className="col-md-6">
							<div className="standard-frame HomepageContents-explaining-item HomepageContents-explaining-item-04">
								<div className="title-destak">Always encrypted</div>
								Have encrypted communications even when your users do not explicitly use https protocol by using <b>TrustCentral</b> proxy services.
							</div>
						</div>
					</div>
				</section>
				<section className="HomepageContents-business-models">
					<h2>Use our cloud servers to improve your security</h2>
					<h2>or have TrustCentral easily installed in your own datacenter.</h2>
				</section>
			</div>
		)
	}
}