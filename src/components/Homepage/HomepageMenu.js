import React from 'react';

export default class HomepageMenu extends React.Component {
	render() {
		return(
			<ul>
				<li className="App-top-menu-item">
					<a className="btn btn-default btn-yellow" href="/">
						<span className="glyphicon glyphicon-home" aria-hidden="true"></span> Homepage
					</a>
				</li>
				<li className="App-top-menu-item">
					<a className="btn btn-default btn-yellow" href="/docs">
						<span className="glyphicon glyphicon-book" aria-hidden="true"></span> Documents
					</a>
				</li>
				<li className="App-top-menu-item">
					<a className="btn btn-default btn-yellow" href="/login">
						<span className="glyphicon glyphicon-user" aria-hidden="true"></span> Login
					</a>
				</li>				
			</ul>
		)
	}
}