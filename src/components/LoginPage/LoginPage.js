import React from 'react';

import LoginFrame from '../LoginFrame/LoginFrame';

import Passwd from '../../plugins/auth/Passwd';

import './LoginPage.css';

export default class Login extends React.Component {
  render() {
    return(
      <div className="LoginPage-background">
      	<LoginFrame plugin={Passwd} />
      </div>
    )
  }
}