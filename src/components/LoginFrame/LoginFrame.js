import React from 'react';

import './LoginFrame.css';

export default class LoginFrame extends React.Component {
	render() {
  		return(
  			<div className="container-fluid">
  				<div className="col-md-3">
  					<div className="well well-sm">
  						{this.props.plugin}
  					</div>
  				</div>
  			</div>
  		)
  	}
}

