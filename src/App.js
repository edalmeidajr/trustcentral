import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Homepage from './pages/Homepage';

import './assets/styles/App.css';


export default class App extends React.Component {
	render() {
		return(
			<BrowserRouter>
		    	<Switch>
		        	<Route exact path="/" component={Homepage} />
		    	</Switch>
			</BrowserRouter>
		)
	}
}
